import com.sun.org.glassfish.gmbal.Description;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TestApi {

    @Test
    @Description("Проверить, что поле count равно 61")
    public void Count61Test() {
        int count61 = 61;
        RestAssured.given()
                .baseUri("https://swapi.co")
                .basePath("/api/planets/")
                .contentType(ContentType.JSON)
                .when()
                .get()
                .then()
                .body("count", Matchers.equalTo(count61))
                .extract()
                .response()
                .prettyPrint();
    }

    @Test
    @Description("Из запроса достать url первой планеты. Выполнить get полученного url. Проверить что status code 200.")
    public void FirstPlanetUrl() {

        List<String> residents = RestAssured.given()
                .baseUri("https://swapi.co")
                .basePath("/api/planets/")
                .contentType(ContentType.JSON)
                .when()
                .get()
                .then()
                .extract()
                .body()
                .jsonPath()
                .getList("results.residents[0]");

        String first = residents.iterator().next();

        RestAssured.given()
                .baseUri(first)
                .contentType(ContentType.JSON)
                .when()
                .get()
                .then()
                .body("name", Matchers.equalTo("Leia Organa"))
                .extract()
                .response()
                .prettyPrint();
    }

    @Test
    @Description("Из запроса достать названия фильмов titles ")
    public void FilmsTest(){
        List<String> films = RestAssured.given()
                .baseUri("https://swapi.co/api/films/")
                .contentType(ContentType.JSON)
                .when()
                .get()
                .then()
                .extract()
                .body()
                .jsonPath()
                .getList("results.title");
        System.out.println(films);
    }

/**    @Test
    @Description("Использовать логин для любого метода post и выполнить post")
    public void LoginForPost(){
        Map<String, String > data = new HashMap<String, String>();
        data.put("username","v.kurchenko");
        data.put("password","123456789");
        RestAssured.given()
                .baseUri("https://openapi.appcenter.ms")
                .basePath("/v0.1/user/invitations/orgs/arsenal/reject")
                .header("X-API-Token", "arsenal")
                .contentType(ContentType.JSON)
                .body(data)
                .when()
                .post()
                .then()
                .statusCode(204)
                .extract()
                .response()
                .prettyPrint();
    }*/
}